//往下滑動增加nav
$(function(){  
  $(window).scroll(function(){
    //var $(window).scrollTop(); 為 scroll
    var scroll = $(window).scrollTop();
    //當卷軸超過70px，自動加上 .navbar-fixed-top ，如果小於就移除
    if( scroll >= 100){      
      $(".navbar.navbar-default").addClass("navbar-fixed-top").slideDown();      
    }
    else{
      $(".navbar.navbar-default.navbar-fixed-top").removeClass("navbar-fixed-top");
    }    
  });
})
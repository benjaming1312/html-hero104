$(document).ready(function(){
      $('.mads-li').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        items:1,
        dots:true,
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayHoverPause:false
      }); 
      // 篩選
      //all
        $('.city li a[data-cityname="city_a"]').click(function(){
            ($('.responsive-table tr td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_1"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_1"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_2"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_2"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_3"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_3"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_4"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_4"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_5"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_5"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_6"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_6"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_7"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_7"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_8"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_8"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_9"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_9"] td')).fadeIn(1000);
        })
        $('.city li a[data-cityname="city_10"]').click(function(){
            ($('.responsive-table tr td')).fadeOut(500);
            ($('.responsive-table tr.head td')).fadeIn(500);
            ($('.responsive-table tr[data-clicktoggle="city_10"] td')).fadeIn(1000);
        })
      // 產品詳情輪播 
      $('.thumb-large > div').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        items:1,
        dots:true,
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayHoverPause:false
      });   
      $('.mpro-wrap .mpgtitle.mpro-name').after($('.page-product'));
    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.right-button').fadeIn(500)
      } else {
        $('.right-button').fadeOut(500)
      }
    });
    //gotop
    $('.right-button').click(function () {
      $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    //append 最新消息模組
    $('.newslist').append($('.newsappend'));
        if ($(window).width() > 767) {

        //把控制項加入陣列       
        var fadein_index=[
                ".product-link .col-sm-6",
                ".newslist",
                ".foot"
                ];
        //滾動animation
        //當視窗高度到達物件時的動作
        function scrollanimation(a, b) {
            $(a).each(function() {
                // Check the location of each desired element //
                var objectBottom = $(this).offset().top + $(this).outerHeight();
                var windowBottom = $(window).scrollTop() + $(window).innerHeight() + 300;
                if (objectBottom < windowBottom) {
                    $(this).addClass(b);
                }
            });
        };

        //遇到class+animation
        $(window).on("load", function() {
              fadein_index.forEach(function(e){
              });       
            $(window).scroll(function() {
                //複製以下新增
                fadein_index.forEach(function(e){
                        scrollanimation(e,'animated fadeIn');
                });

            });
        });
    }
  })
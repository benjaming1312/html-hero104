/*把手刻的footer放到預設footer內*/
    $('footer .pull-left.copyright').before($('footer.container-fluid.foot'));
//輪播圖新增calss name控制項
        $('#homebody .flexslider').addClass('banner');
//移除預設無連結點擊功能
       $("[class^='col-md-12'] img").parent("a[href='']").removeAttr('href');
       $("[class^='col-md-12'] img").parent("a[href='#']").removeAttr('href');
       $("[class^='col-md-12']  a[href='http://www.shortday.in/wp-content/uploads/2015/05/emma-stone-beautiful-wallpaper2.jpg']").removeAttr('href');
/*手寫footer放入預設footer*/
        $('#wrap > footer').append($('div.container-fluid.foot'));
        
/*產品分類頁特別指定Class*/
        $('#wrap .productlist').parent().parent().parent('body#pagebody').addClass('product_page');
        
/*預設menu-wrapper加一個class*/
        $('.menu-wrapper').addClass('container');

/*商品明細搬遷*/
        $('.product-info .product-title').after($('.productlist .product-detail'));

/*contact 只在特定頁面顯示*/
		//取得網址的值
        var contact_href = window.location.toString();
		//如果網址裡面有contact的文字，加入class
        if (contact_href.indexOf("contact") != -1){
            $('body#pagebody').addClass('contact_page');
        }
//預設分享文字修改
        $('#h4Share').text('Share');

//商品分類頁修改[分類]名稱
        $('#ContentPlaceHolder1_h3catetitle').text('Category');

//商品分類頁搜尋內容改英文
        $('#txtSearch').attr('placeholder',' Please Input Keyword');

//聯絡我們第一個欄位新增文字內容
        $('#txtName').attr('placeholder','Your Name');

//聯絡我們第二個欄位新增文字內容
        $('#txtMobile').attr('placeholder','Your Phone');

//聯絡我們第三個欄位新增文字內容
        $('#txtEmail').attr('placeholder','Your Email');

//聯絡我們第四個欄位新增文字內容
        $('#ContentPlaceHolder1_txtText').attr('placeholder','Company Name');

//聯絡我們第五個欄位修改文字內容
        $('textarea#txtContext').attr("placeholder","Please feel free to contact us if you have any inquiry. We would be glad to receive your message. Thank you! Have a nice day!");

//聯絡我們第送出訊息修改文字內容
        $('#btnSend').attr("value","Send Meassage");    
 
//手機版本
    if ($(window).width() > 768) {      
/*系統logo放入左側*/
    $('.container-fluid.menu-wrapper-area .menu-wrapper').before($('#wrap #main-nav .nav-header'));
        }   